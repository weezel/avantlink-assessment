(function(){
  'use strict';

  angular.module('tasks')
         .service('taskService', ['$q', '$http', '$resource', TaskService]);

  function TaskService($q, $http, $resource){

    // this.taskList = function() {
    //   return $http.get('https://avantlink-rest.herokuapp.com/api/tasks')
    //   .then(function(response) {
    //     return response.data;
    //   })
    //   .catch(function(err){
    //       console.log("Error getting task list: ", err);
    //   });
    // }

    return $resource("https://avantlink-rest.herokuapp.com/api/tasks/:id");

  }
})();
