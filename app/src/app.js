angular
    .module('avantlinkApp', ['ngMaterial', 'tasks', 'ngResource'])
    
    .config(function($mdThemingProvider){
        $mdThemingProvider.theme('default')
            .primaryPalette('blue')
            .accentPalette('deep-orange');
    })

    .controller('viewsCtrl', function ($rootScope, $mdToast) {
        var self = this;

        //Toast Functions
        $rootScope.showToast = function(msg) {
            $mdToast.show(
            $mdToast.simple()
                .textContent(msg)
                .position('bottom right')
                .hideDelay(3000)
            );
        };

    });