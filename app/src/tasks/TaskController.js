(function () {

  angular
    .module('tasks')
    .controller('TaskController', [
      'taskService', '$timeout', '$log', '$mdDialog', '$resource', '$scope', '$rootScope',
      TaskController
    ]);

  function TaskController(taskService, $timeout, $log, $mdDialog, $resource, $scope, $rootScope) {
    var self = this;

    self.selected = null;
    self.tasks = [];

    self.taskQuery = taskService.query();

    //Get all tasks from REST service.
    self.taskQuery.$promise
    .then(function(data) {
        self.tasks = data;
        self.selected = self.tasks[0];
    });

    //Set the selected task on the page.
    self.selectTask = function(task) {
      self.selected = angular.isNumber(task) ? self.tasks[task] : task;
    }

    //Function to show dialog for deleting a new task.
    self.confirmDeleteTask = function(ev, taskId) {
      var confirm = $mdDialog.confirm()
        .title('Delete this task?')
        .textContent('Your task will be deleted.')
        .ariaLabel('Delete task confirmation')
        .targetEvent(ev)
        .ok('DELETE')
        .cancel('CANCEL');

      $mdDialog.show(confirm).then(function () {
        self.deleteTask(taskId);
      }, function () {
        // console.log("Task not deleted.");
      });
    };

    //Delete selected task.
    self.deleteTask = function(taskId) {
      taskService
        .delete({ id: taskId })
        .$promise
        .then(function(data) {
          //Remove the deleted task from the array.
          self.removeItemByID(taskId, self.tasks);
          //Reset the selected task to be the first one in the array.
          self.selected = self.tasks[0] != null ? self.tasks[0] : null;
          $rootScope.showToast("Task deleted!");
      });
    };

    //Use these next 2 functions to find the object to remove from the tasks array.
    self.findItemByID = function(id, items) {
      for(var i = 0; i < items.length; i++) {
        if(items[i].id === id) {
          return items[i];
        }   
      }   
      return null;
    };

    self.removeItemByID = function(id, items) {    
      var item = self.findItemByID(id, items);    
      if(item) {
        items.splice(items.indexOf(item), 1);    
      } 
    };

    //Function to show dialog for adding a new task.
    $scope.showAddTaskDialog = function (ev) {
      $mdDialog.show({
        controller: AddTaskController,
        templateUrl: './src/tasks/view/addTask.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: true,
        fullscreen: false
      })
      .then(function (task) {
        taskService
          .save({
            task_name: task.task_name, 
            task_desc: task.task_desc
          })
          .$promise
          .then(function(data) {
            //Add new task to array and set selected to new task.
            self.tasks.push(data);
            self.selected = self.tasks[self.tasks.length-1];
            //Show success toast message.
            $rootScope.showToast("New task added.");
        });
      }, function () {
        //cancelled dialog
      });
    };

    function AddTaskController($scope, $mdDialog) {
      $scope.hide = function () {
        $mdDialog.hide();
      };
      $scope.cancel = function () {
        $mdDialog.cancel();
      };
      $scope.addTask = function (task) {
        $mdDialog.hide(task);
      };
      $scope.editTask = function (task) {
        $mdDialog.hide(task);
      };
    }

  }

})();
